package main
import objectandclass.classMatching
import objectandclass._
import inheritance.Child
import abstracts.Developer
import interfaces.Student

object Main {
  
  def main(args : Array[String]){
    
    var obj=new Class1();
    obj.sayHello("hello lucky h r u?")
    
//    obj.sayHello(10.2356)
    
    new Class1(10,"lucky").toStringClass1()
    
     Companion().log("Companion Object")
    
    var caseClassObj= CaseClass(10,20)
    
    println("caseClassObj to String :: "+caseClassObj.toString())
    
    //Inheritance demo
    
    val c =new Child();
    print("basic: "+c.basic)
    println(" \thra: "+c.hra)
    
    var dev= new Developer()
    println("Developer Basic : "+dev.basic())
    

    var student= new Student()
    println("collage: "+student.university()+"\t dept: "+student.collageDept())
    
    
  }
}