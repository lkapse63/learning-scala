package main

import java.io.{File, PrintWriter}

class FileSystem {

  final def listFiles(base: File, recursive: Boolean = true): Seq[File] = {
    val files = base.listFiles
    val result = files.filter(_.isFile)
    result ++
      files
        .filter(_.isDirectory)
        .filter(_ => recursive)
        .flatMap(listFiles(_, recursive))
  }

}

object FileSystem {

  def apply(): FileSystem = new FileSystem()

  def main(args: Array[String]): Unit = {
    val file = (new File("."))
    /*withPrintWriter(new File("resources/filename.txt")) {
      writer => {
        val f = FileSystem.apply()
        val files = f.listFiles(file, true)
        files
          .filter(_.toString.contains("src"))
          .map(_.getName)
          .foreach(l => writer.write(l + "\n"))
      }
    }*/
    withPrintWriter(new File("resources/filename.txt")) {
      printWriterLiteral
    }
  }

  def withPrintWriter(file: File)(op: PrintWriter => Unit): Unit = {
    val writer = new PrintWriter(file)
    try {
      op(writer)
    } finally {
      writer.close()
    }
  }

  def printWriterLiteral(printWriter: PrintWriter): Unit = {
    val f = FileSystem.apply()
    val files = f.listFiles(getFile("."), true)
    files
      .filter(_.toString.contains("src"))
      .map(_.getName)
      .foreach(l => printWriter.write(l + "\n"))
  }

  val getFile = (dir: String) => (new File(dir))
}
