package main

import java.io.File

class FileMatching {

  val files = () => scala.io.Source.fromFile(new File("resources/filename.txt"))

  def filePatternMatching(f:(String) => Boolean) = {
      files()
        .getLines()
        .filter(f)
        .mkString("\n")
  }

  def findStartWith(query:String) = {
     filePatternMatching(_.toLowerCase.startsWith(query))
  }

  def findContains(query:String) = {
    filePatternMatching(_.toLowerCase.contains(query))
  }

  def findByEndsWith(query: String) = {
    filePatternMatching(_.endsWith(query))
  }

}

object FileTest extends App{
    val fileMatching = new FileMatching()
    println("Search by start with ::  i")
    println(fileMatching.findStartWith("i"))
    println("#######################################")
    println("Search by contains ::  java")
    println(fileMatching.findContains("java"))
    println("#######################################")
    println("Search by ends with ::  scala")
    println(fileMatching.findByEndsWith(".scala"))

  val lst = List(1,2,3,4,5,6,7,8,-1)
  lst.exists(_ < 0)
}
