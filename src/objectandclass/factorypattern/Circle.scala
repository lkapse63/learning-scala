package objectandclass.factorypattern

class Circle extends Shape {
  override def draw(): Unit = println("this is circle")
}
