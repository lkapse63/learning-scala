package objectandclass.factorypattern

trait Shape {

  def draw() : Unit

}
