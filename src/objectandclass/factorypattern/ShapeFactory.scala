package objectandclass.factorypattern

class ShapeFactory private {

    def getShape(beanName: String) : Option[Shape] = beanName match {
    case "circle" => Option(new Circle())
    case "tringle" => Option(new Tringle())
    case _ => Option.empty
  }

}

object ShapeFactory{
   private val shapeFactory =new ShapeFactory()
   def getShape(beanName:String) = shapeFactory.getShape(beanName)
}
