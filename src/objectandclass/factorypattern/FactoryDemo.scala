package objectandclass.factorypattern

object FactoryDemo {

  def main(args: Array[String]): Unit = {

    val shape = ShapeFactory.getShape("circle")
    shape
      .foreach(s => s.draw())
  }

}
