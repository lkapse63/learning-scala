package objectandclass

class  Companion private(name:String) {
  
  def log(msg: String) :Unit= println(msg)


  override def toString: String = "["+ name + "]"
}


object Companion{

  def apply(): Companion = new Companion("Default")
  def apply(name: String): Companion = new Companion(name)
}