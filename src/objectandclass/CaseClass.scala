package objectandclass

trait SuperTrait
case class CaseClass(a: Int, b: Int) extends SuperTrait
case class CaseClass2(c: Int) extends SuperTrait
case class CaseClass3() extends SuperTrait

object classMatching {

  def main(args: Array[String]) {

    callCase(CaseClass(10,20)) 
    callCase(CaseClass2(10))
    callCase(CaseClass3())

    val p1 = Person("lucky", 29.5)
    val p2 = Person("prajaka", 29.5)
    val p3 = Person("prajaka", 29.5)

    println("object comparison:: "+(p3==p2))
  }

  def callCase(classref: SuperTrait) = classref match {

    case CaseClass(in1, in2) => println("in1: " + in1 + " in2: " + in2)

    case CaseClass2(in1)     => println("in1: " + in1)

    case _                   => println("No arg class")
  }
}
 