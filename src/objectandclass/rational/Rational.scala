package objectandclass.rational

class Rational(private val n:Int,private val d:Int) {
  require(d > 0, "d must be greater than 0")
  override def toString: String = numer +"/"+ denom

  private val g = gcd(n.abs,d.abs)
  val numer = n / g
  val denom= d / g

  def this(n:Int) = this(n,1)  //auxiliary constructor

  def add(that: Rational):Rational ={
     new Rational(
       this.numer * that.denom + that.numer * this.denom,
       that.denom * this.denom
    )
  }

  def + (that: Rational) = this.add(that)

  def + (i :Int) = this.add(new Rational(i,1))  // method overloading

  def minus(that: Rational):Rational ={
    new Rational(
      this.numer * that.denom - that.numer * this.denom,
      that.denom * this.denom
    )
  }

  def - (that : Rational) = this.minus(that)

  def - (i : Int) = this.minus(new Rational(i,1))

  def lessThan(that: Rational):Boolean ={
    this.numer * that.denom < that.numer * this.denom
  }

  def < (that: Rational) = this.lessThan(that)

  def max(that: Rational) = if(this.lessThan(that)) that else this

  def mul(that:Rational) = new Rational(this.numer * that.numer, this.denom * that.denom)

  def * (that: Rational) = mul(that)

  private def gcd(a:Int, b :Int) :Int ={
    if(b==0) a else gcd(b,a % b)
  }

}

object Rational{
  def apply(n: Int, d: Int): Rational = new Rational(n, d)
  def apply(n: Int): Rational = new Rational(n, 1)
}
