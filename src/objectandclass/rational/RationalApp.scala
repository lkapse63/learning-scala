package objectandclass.rational

object RationalApp {

  def main(args: Array[String]): Unit = {
    val oneHalf = Rational(1, 2)
    val twoThird = Rational(2, 3)
    val five = Rational(5)

    println(oneHalf + twoThird)
    println(oneHalf < twoThird)
    println(oneHalf max five)
    println(oneHalf * five)
    println(Rational(66, 42))
    println(oneHalf + twoThird * five) // operator precedence
    println((oneHalf + twoThird) * five)

    println(oneHalf + 2)

    implicit def intToRational(x: Int) = new Rational(x, 1) // implicit conversion
    println(2 - oneHalf)

  }
}
