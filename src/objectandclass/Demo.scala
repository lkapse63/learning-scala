package objectandclass

object Demo {
  private val demo = new Demo()
  def incr(inc : Int=1):Int = {
    demo.add(inc)
    demo.get()
  }

  def apply(in:Int): Int = incr(in)
  def apply(in:Int,step:Int): Int = incr(in)+incr(step)
}

class Demo private {
  private var  count=0
  private def add(incr : Int){ count =count+incr}
  private def get() : Int = count
}
