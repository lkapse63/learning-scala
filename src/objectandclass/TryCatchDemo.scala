package objectandclass

object TryCatchDemo {

  def main(args: Array[String]): Unit = {
    try {
      div(2, 1)
      throw new NumberFormatException("number format")
    } catch {
      case e: ArithmeticException => println(e)
      case e: Exception => println("exception")
    }
    finally {
      println("finally code")
    }
    println("rest of code")

    val a=1;
    {
      val a=2 // don't write such things
    }

  }

  def div(x: Int, y: Int) = x / y

}
