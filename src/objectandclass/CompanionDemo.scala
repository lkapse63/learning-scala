package objectandclass

object CompanionDemo {

  def main(args: Array[String]): Unit = {
    println("Companion demo:: "+Companion())
    println("Companion demo:: "+Companion("lucky"))

    println("lucky checksum: "+ChecksumAccumulator.calculate("lucky"))
    println("Every value has a checksum "+ChecksumAccumulator.calculate("Every value has a checksum"))

    val str="Every value has a checksum"

    println("demo incr : "+Demo.incr())
    println("demo incr : "+Demo.incr())
    println("demo incr : "+Demo.incr())
    println("demo incr : "+Demo.incr())
    println("demo incr : "+Demo.incr())
    println("demo incr : "+Demo.incr(3))
    println("demo incr : "+Demo.incr())

    val incr = Demo.incr _
    println(incr.apply(1))
    println(Demo.apply(1,2))
  }

}
