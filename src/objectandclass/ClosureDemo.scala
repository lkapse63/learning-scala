package objectandclass

import java.io.{File, PrintWriter}
import java.util.Date
import scala.Double

class ClosureDemo {
  private def filesHere = (new File("C:/Users/a818517/Documents/bugs")).listFiles()
  private def fileMatching(matcher : String => Boolean) = {
    for(file <- filesHere if matcher(file.getName)) yield file
  }

  def fileEndsWith(query : String) = fileMatching(_.endsWith(query))
  def fileStartsWith(query : String) = fileMatching( file => file.startsWith(query))
  def findWithRegex(query : String) = fileMatching( file => file.matches(query))

  def isEven(n : Int) : Boolean = n % 2 == 0

  def find[A](list : List[A], p : A => Boolean) : Option[A] = {
    var pList = list
    while (!pList.isEmpty){
      println("checking for "+pList.head)
      if(p(pList.head)) return Some(pList.head)
      else pList = pList.tail
    }
    None
  }

  def find2[A] (list : List[A], p : A => Boolean) : Option[A] = {
    if( list.isEmpty || p(list.head)) if (list.isEmpty) None else Some(list.head)
    else find2(list.tail,p)
  }

  def find3[A] (list : List[A]) (p : A => Boolean) : Option[A] = {
    if( list.isEmpty || p(list.head)) if (list.isEmpty) None else Some(list.head)
    else find3(list.tail)(p)
  }

  val  findFirstEven = find3(_:List[Int])((x:Int) => x % 2 == 0)

  def findAll[A]( list: List[A], p : A => Boolean): List[A] = for(l <- list if p(l)) yield l

  val findAllEvenVal: List[Double] => List[Double] = findAll(_: List[Double], (x: Double) => x % 2 == 0)

  def findEven[A <: Double](list: List[A]) = {
     findAll(list, (x:A) => x % 2 == 0)
  }

  def withFileWriter(file : File) (op : PrintWriter => Unit): Unit ={
    val writer = new PrintWriter(file)
    try{
      (1 to 10).foreach(itr => {
        op(writer)
        Thread.sleep(1000)
      })
//      op(writer)
    } catch {
      case ex  : Exception => println("got exception "+ex)
    }
    finally {
      writer.close()
    }
  }



  def find(list: List[Int])={
    list match {
      case List(_,3,_*) => "At second position 3"
      case elem if elem(1) % 2 == 0 => "second even"
      case elem if elem(1) % 2 != 0 => "second odd"
      case _ => "Nothing"
    }

  }
  var isAssertionEnabled = true
  def byName(op: => Boolean)={
    if(isAssertionEnabled && !op) throw new AssertionError("byName : condition false")
    // do nothing
  }
  def byValue(op: Boolean)={
    if(isAssertionEnabled && !op) throw new AssertionError("byValue : condition false")
    // do nothing
  }
}

object ClosureDemo{
  def apply() = new ClosureDemo()
  def main(args: Array[String]): Unit = {
    val closer = ClosureDemo()
    println(closer.fileEndsWith(".txt").mkString("\n"))
    println("==============================================================")
    println(closer.fileStartsWith("new_").mkString("\n"))
    println("==============================================================")
    println(closer.findWithRegex("([bosch])").mkString("\n"))

    println(closer.find2(List("abc","xyz","pqr","lmn"), (x : String) => x == "lmn2").orElse(Some("Not There")).get)

    val inList=List(1,3,5,7,9,8,2)
    println("find first even "+ closer.findFirstEven(inList))
    println("find all  even "+ closer.findAllEvenVal(inList.map(_*1.0)));
    println(closer.find(inList))

     /*closer.withFileWriter(new File("./date.txt")){
      writer => writer.println(new Date().toString)
    }*/

    val byName = closer.byName(_)
    val byValue = closer.byValue(_)

    println(byName(5 > 3))
    println(byValue(5 > 3))
    closer.isAssertionEnabled=false
    println(byName(5 / 0 == 0))
    println(byValue(5 / 0 == 0))
  }

}
