package objectandclass

class Class1{
  println("constructor  init")
  var a: Int=0
  var s: String=null
  
  def this(a: Int, s : String){
    this()
    this.a=a
    this.s=s
    
     println("Aux constructor  init")
    
  }
  
 def sayHello(): Unit = println("Hello World: class example")
 
 def toStringClass1() : Unit = println("a: "+a+" b "+s)
 
 
 //Method overloading
 
  def sayHello(msg : String): Unit = println(msg)
  
   def sayHello(msg : Double): Unit = println(msg)
}