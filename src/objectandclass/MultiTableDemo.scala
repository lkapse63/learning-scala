package objectandclass

object MultiTableDemo {

  def main(args: Array[String]): Unit = {
    println(MultiTableDemo.multTable())
  }

  def makeRowSeq(row: Int) = {
    for (col <- 1 to 10)
      yield {
        val prod = (row * col).toString
        val padding = " " * (4 - prod.length)
        prod + padding
      }
  }

  def makeRow(row: Int) = makeRowSeq(row).mkString

  def multTable() = {
    val tableSeq = for (row <- 1 to 10)
      yield makeRow(row)
    tableSeq.mkString("\n")
  }


}
