package geekforgeeks.customecollection

class MyList {

  var node:Node = _
   class Node(number:Int){
    var head:Node = _
     def getNumber:Int = number
    override def toString = number.toString
  }

  def add(number: Int): Unit = {
    var n = new Node(number)
    if (node == null)
      node = n
    else {
      n.head=node
      node=n
    }
  }

  private def printNode(node:Node):Unit={
    if (node != null) {
      println(node.toString)
      printNode(node.head)
    }

  }

  def printAll(): Unit ={
    printNode(node)
  }

  def foreach(fun: Int => Unit, n:Node=node):Unit={
    if(n!=null) {
      fun(n.getNumber)
      foreach(fun,n.head)
    }
  }

  def map(fun:Int=>Int, n:Node=node):this.type ={
     if(n!=null){
       println("map function::"+fun(n.getNumber))
       map(fun,n.head)
     }
    this
  }


}

object TestMyList{
  def main(args: Array[String]): Unit = {
    val list =new MyList();
    list.add(1)
    list.add(2)
    list.add(3)

//    list.printAll()

    def print(n : Int):Unit={
      println(n.toString)
    }
//    list.foreach(print)

    list
      .map(x => x+1)
      .foreach(print)
  }
}
