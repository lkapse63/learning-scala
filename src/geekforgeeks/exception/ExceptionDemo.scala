package geekforgeeks.exception

object ExceptionDemo {

  def main(args: Array[String]): Unit = {

    val a:Int=10
    val b:Int=0

    try{
      val c= div(a,b)
      println(c)
    }catch{
      case ex:ArithmeticException => println("ArithmeticException:: "+ex)
      case ex2:Exception => throw ex2
    }finally {
      println("Always excecuted")
    }
    println("rest of code")

    throw new CustomeException("my Exception")

  }

  @throws(classOf[ArithmeticException])
  def div(a:Int , b : Int):Int  = a/b
}
