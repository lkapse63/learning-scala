package geekforgeeks.exception

object EigtherDemo {

  def main(args: Array[String]): Unit = {
    println(namePrint("lucky"))
    println(namePrint(""))
  }

  def namePrint(name:String):Either[String,String]={
    if(name.isEmpty)
      Left("There is no name")
    else
      Right(name)

  }
}
