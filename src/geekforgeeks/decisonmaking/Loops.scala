package geekforgeeks.decisonmaking

import scala.reflect.macros.whitebox

object Loops {

  def main(args: Array[String]): Unit = {

    println("output of while")
    whileFun()
    println("output of do while")
    doWhileFun()
    println("output of for loop")
    forLoopFun()
    println("output of two for")
    twoFor()
    println("For with if condtion in looping")
    forIf()
    println("Output of for yeild")
    forYield()
    println("output of power function")
    println(power(2,4))
    println("output of while power function")
    println(whilePower(2,4))
    println("output of list iterator")
    listIterator()
  }

  /**
   * While loop demo
   */
  def whileFun(): Unit ={
    var a =1
     while (whileCondtion(a)){
       println(a)
       a=a+1
     }
  }

  val whileCondtion = (a : Int) => a > 5

  /**
   * Do while loops
   */
  def doWhileFun(): Unit ={
    var a =1
    do{
      println(a)
      a=a+1
    }while (whileCondtion(a))
  }

  /**
   * For loop demo
   * Syntax
   * for(w <- Range){
   * //Statement
   * }
   */
  def forLoopFun(): Unit ={

      for(x <- forloopRange2())
        println(x)
  }

  val forloopRange1 = () => 1.until(5,1)
  val forloopRange2 = () => Range(1,5,1 )
  val forloopRange3 = () => 1 to 5

  /**
   * for loop with two condtions
   */
  val  twoFor = () => {
    for(x <- 1 to 5 ; y <- 1 to 2)
      println("x :",x," y : ",y)
  }

  /**
   * for with if condtion
   */
  val forIf = () => {
      for(x <- 1 to 10 ; if x > 3; if x < 8)
        println(x)
  }

  /**
   * For with yield
   */
  val forYield = () => {
    var output = for (x <- 1 to 5) yield {x * 2}
    println(output)
  }

  def power(base: Int, exp: Int): Int = {
    var pow: Int = 1
    for (x <- 1 to exp) {
      pow = pow * base
    }
    pow
  }

  /**
   * Power function using
   * while loop
   */
  def whilePower(base:Int, exp : Int):Int = {
      var iexp= exp
    var pow:Int=1
    while(iexp > 0){
      pow =pow *base
      iexp = iexp-1
    }
    pow
  }

  /**
   * For loop
   * Iterating over list
   */
  def listIterator = () => {

      val list = List(1,2,3,4,5)
    for(x <- list; if x % 2 ==0 )
      println(x)
  }
}
