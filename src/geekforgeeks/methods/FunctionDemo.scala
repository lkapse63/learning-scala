package geekforgeeks.methods

object FunctionDemo {
  /**
   * scala method sysntax
   *  def method_name(variable_name: Data_Type..) : return_type ={ code// }
   */

  def main(args: Array[String]): Unit = {
    println("add "+fun1(10,20))
    println("sub "+highFun(10,20,_ - _))
    println("mul "+highFun(10,20,_ * _))
    println("div "+highFun(10,20,_ / _))
  }

  def fun1(a:Int,b:Int):Int={
    a+b
  }

  val highFun = (num1 : Int, num2 : Int , f : (Int,Int)=>Int) => f(num1,num2)
}
