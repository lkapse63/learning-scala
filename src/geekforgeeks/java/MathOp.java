package geekforgeeks.java;

public class MathOp {

    public void add(int a,int b){
        System.out.println("addition is:: "+(a+b));
    }

    public void sub(int a,int b){
        System.out.println("subtraction is:: "+(a+b));
    }

    public void mul(int a,int b){
        System.out.println("multiplication is:: "+(a+b));
    }

    public void div(int a,int b){
        System.out.println("division is:: "+(a*1.0)/(b*1.0));
    }

    /**
     *
     * @param arr
     * @param index
     * @return
     * Java recursion code...
     */
    public int sumOfX(int[] arr, int index){
        if(arr.length <= index)
            return 0;
        else
            return arr[index]+sumOfX(arr,index+1);
    }
}
