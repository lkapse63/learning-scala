package geekforgeeks.java

object JavaScalaDemo {

  def main(args: Array[String]): Unit = {
    val mathOp : MathOp = new MathOp()

    mathOp.add(10,20)
    mathOp.sub(10,20)
    mathOp.mul(10,20)
    mathOp.div(10,20)

    val arr = Array[Int](1,2,3,4,5,6,7,8,9)
    println("java Recursion::"+mathOp.sumOfX(arr,0))
  }
}
