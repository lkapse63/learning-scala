package geekforgeeks.basic

object PatternMatching {

  def main(args: Array[String]): Unit = {

    println(test(1))

    val dog : Dog = new Dog("filipa")
    println("class pattern matching ",test2(dog))
  }

  def test(x:Int):String=x match {
    case 0 => "case 0 executed"
    case 1 => "case 1 executed"
    case _ => "default case executed"
  }


  trait  Animal
  case class Dog(breed:String) extends Animal
  case class Cat(breed:String) extends Animal

  def test2(animal: Animal) : String = animal match {
    case Cat(breed) => "its a Cat"
    case Dog(breed) =>"its a Dog"
    case _ =>"may be other category"
  }
}
