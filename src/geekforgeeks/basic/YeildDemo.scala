package geekforgeeks.basic

object YeildDemo {

  /**
   * Syntax for yield
   *  val output= for(x <- List) yield x
   */
  def main(args: Array[String]): Unit = {
        println("power ",power(2,4))
        println("factorial:: ",factorial(3))

  }

  val power = (base: Int, exp: Int) => {
    val tmp = for (x <- 1 to exp) yield base
    var pow = 1;
    for (i <- tmp) pow = pow * i
    pow
  }

  val factorial : Int => Int = (num : Int)  => if(num == 0) 1 else num * factorial(num-1)

  /*def factorial(num : Int ): Int ={
    if(num == 0) 1 else num * factorial(num-1)
  }*/
}

