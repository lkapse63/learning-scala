package geekforgeeks.basic;

public class PassByValue {

    public static void main(String[] args) {
        int a=10;
        int b=20;
        PassByValue passByValue =new PassByValue();
        int add = passByValue.add(a, b);
        System.out.println(a);
    }

    public int add(int a, int b){
        a = a+b;
        return a;
    }
}
