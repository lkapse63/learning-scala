package geekforgeeks.basic

object DataTypes {

  def main(args: Array[String]): Unit = {

     var b: Boolean= false
    println("boolean variables b ",b)

    val s:Short= -12
    println("short is ", s)

    val i:Int=20
    val u:Unit=()
    println("unit u",u)

    val(name1:Int, name2:String) = (2, "geekforgeeks")
    println("Nothing ",name2)

    val line=scala.io.StdIn.readLine()
    println("read line ",line)
  }


}
