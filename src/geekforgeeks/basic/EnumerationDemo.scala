package geekforgeeks.basic

object EnumerationDemo {

  def main(args: Array[String]): Unit = {
    println(EnumDemo.first)

    EnumDemo.values.foreach({
      case d if (d == EnumDemo.first) => println("fave category ",d)
      case _ => None
    }

    )
  }
}

object EnumDemo extends Enumeration{

  val first = Value("Comedy")
  val second = Value("Action")
  val third = Value("Romantic")
}