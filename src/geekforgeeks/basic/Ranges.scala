package geekforgeeks.basic

object Ranges {

  def main(args: Array[String]): Unit = {
     val r1 = Range(1,10, 2)

    val r2 = r1.dropWhile(p => condiation(p))
    println(r2)

    val u1 = 1 until 10 by 2
    println(u1)

    val u2 = 1 until 15 by 1
    println(u2)

    val t1= 1 to 8
    println("by to ",t1)
  }

  val condiation  = (i:Int) => i < 5
}
