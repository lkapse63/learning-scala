package geekforgeeks.basic
import scala.util.control.Breaks
import scala.util.control.Breaks._

object BreakbleDemo {

  def main(args: Array[String]): Unit = {

     breakFor2()

    val s =
      """multiline
        String
        literlas
        """
    println("multiline literals: \n",s)

  }

  def breakFor1(): Unit ={
    breakable (
      {
        for (x <- 1 to 5) {
          for (y <- 1 to 5) {
            if (x == 4)
              break ()
            println ( "value of x::", x, " y:: ", y )
          }

        }
      }
    )
  }

  //Using breakable
  def breakFor2(): Unit ={
    val breakFor= new Breaks()
    breakable (
      {
        for (x <- 1 to 5) {
          println("value of x:: ",x)
          if(x==4)
            breakFor.breakable()
        }
      }
    )
  }
}
