package geekforgeeks.functions

object ClosureDemo {

  def main(args: Array[String]): Unit = {
    val closure = new Closure()
//    val comp=
    println("computation ",closure.getComputation(103))
  }
}

class Closure{
  val e: Map[Int,Double]= Map(100 -> 1000,102-> 1234,103->2145,104->5489)
  val p: Map[Int,Double]= Map(100 -> 10,102-> 12,103->21,104->54)

  def getComputation :Int => (Int,Double)
    = (empId : Int)
    =>{
        (empId, e(empId) * p (empId) / 100.00)
    }:(Int,Double)
}
