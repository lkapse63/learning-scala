package geekforgeeks.functions

object ChainingDemo {

  def main(args: Array[String]): Unit = {
       val p =new Person()
    val name=p.setName("lucky")
              .setLastName("Kapse")
    println("Chaining ",name)
  }
}

class Person {
  private var name:String =""
  private var lName:String =""
  def setName(name:String) : this.type ={
    this.name=name
    this
  }

  def setLastName(lName : String):this.type = {this.lName=lName;this}

  def getName():String  ={
    this.name
  }

  override def toString: String = name+" : "+lName
}
