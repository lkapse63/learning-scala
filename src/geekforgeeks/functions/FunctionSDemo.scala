package geekforgeeks.functions

object FunctionSDemo {

  def main(args: Array[String]): Unit = {
    val ops =op(1)
    println(ops(10,20))
  }


  def op = (mul :Int ) => {
     mul match {
       case 1 => (num1 : Double,num2 : Double) => num1 + num2
       case 2 => (num1 : Double,num2 : Double) => num1 - num2
       case 3 => (num1 : Double,num2 : Double) => num1 * num2
       case 4 => (num1 : Double,num2 : Double) => num1 / num2
       case _ => (num1 : Double,num2 : Double) => println("Nothing to show here...")
     }
  }
}
