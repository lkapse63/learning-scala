package geekforgeeks.functions

object PartailyAppliedFunctionDemo {

  def main(args: Array[String]): Unit = {

    println("Volume of cylinder:: "+calculateVolume(10,15,piHolder))
    println("Volume of cylinder:: "+calculateVolume(10,15,volume(3.4, _:Double, _:Double)))

    /**
     * partially applied function
     */
    def allVariableHolder = piHolder _
    println("pi holder demo :: "+allVariableHolder().apply(2.3,10.8))

    println("Circle of radius:: "+volumeCircle(4/3)(3.14)(3))

    def circleVolume=volumeCircle(4/3)(3.14) _
    println("Curring demo :: "+circleVolume(3))
  }

  /**
   * Partially applied function
   */
  def volumeCircle(constant: Double)(pi:Double)(radius:Double)={
    constant * pi * radius * radius * radius
  }

  def volume(pi:Double,radius:Double,hight:Double):Double={
    pi * radius * radius * hight
  }

  def piHolder = volume(3.4, _:Double, _:Double)

  def calculateVolume(r:Double,h:Double, fun:(Double,Double)=>Double)={
    fun(r,h)
  }
}
