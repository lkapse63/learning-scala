package geekforgeeks.functions

object RecursionDemo {

  def main(args: Array[String]): Unit = {
    def fun(x:Double) = x * x
    val arr=Array[Double](1,2,3,4,5,6,7)
    val arrayOp =new ArrayOp()
    val reduce = arrayOp.myReduce(arr,fun)
    println("Reduce value :: "+reduce)
    println("my map value :: ")
    arrayOp.myMap(arr,x=> x*x*x)
    println("map function with for loop")
    arrayOp.custMap(arr, x=>x/2)
    println("for with yield :: "+arrayOp.custMapYield(arr,x=>x*2.5))
    println("map function with while loop")
    arrayOp.custMapWhile(arr, x=>x/2)

  }
}

class ArrayOp {

  /**
   *  My reduce function for array using recursion
   * @param arr = Input array
   * @param fun = Input function that apply on i/P array
   * @param index = Default index for starting point
   * @return
   */
   def myReduce(arr:Array[Double], fun:Double=>Double,index:Int=0):Double={
      if (arr.length <= index) 0.0 else fun(arr(index)) + myReduce(arr,fun,index+1)
   }


  def myMap(arr:Array[Double], fun:Double=>Double,index:Int=0) :Double={
    if (arr.length <= index) 0.0
    else {
      println(fun(arr(index)))
      myMap(arr,fun,index+1)
    }
  }

  def custMap(arr:Array[Double], fun:Double=>Double): Unit ={
    for(i <- 0 to arr.length-1)
      println(fun(arr(i)))
  }

  /**
   *
   * @param arr
   * @param fun
   * @return
   */
  def custMapYield(arr:Array[Double], fun:Double=>Double)={
    for(i <- 0 to arr.length-1) yield fun(arr(i))
  }

  def custMapWhile(arr:Array[Double], fun:Double=>Double): Unit ={
    var index=0
    while(arr.length>index){
      println(fun(arr(index)))
      index=index+1
    }
  }


}