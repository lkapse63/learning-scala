package geekforgeeks.parameterized

object VarianceDemo {

  def main(args: Array[String]): Unit = {
    val dog = new Dog()
    val puppy = new Puppy()

    val varianceDog= new VarianceAnimal[Dog](dog)
    val variancePuppy= new VarianceAnimal[Puppy](puppy)

    val dogAnimal = new AnimalCarer(varianceDog)
    val puppyAnimal= new AnimalCarer(variancePuppy)
  }
}

class VarianceAnimal[+T](val animal:T)

class Dog
class Puppy extends Dog

class AnimalCarer(val dog:VarianceAnimal[Dog])

