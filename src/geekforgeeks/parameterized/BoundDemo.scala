package geekforgeeks.parameterized

object LowerBoundDemo {

  def main(args: Array[String]): Unit = {
    new CategoryLower[Invertebrates](new Mammals)
    new CategoryUpper[Birds](new Mammals)
  }
}

trait Animal
class Invertebrates extends Animal
class Fish extends Invertebrates
class Amphibians extends Fish
class Reptiles extends Amphibians
class Birds extends Reptiles
class Mammals extends Birds

/**
 * Lower bound
 * @param animal
 * @tparam A
 *  type parameter A either same as Fish or
 *  super class of Fish
 */
class CategoryLower[A >: Fish] (val animal: A)

/**
 * Upper bound
 * @param animal
 * @tparam A
*    type parameter A must be same as Fish
 *    or sub-type of Fish
 */
class CategoryUpper[A <: Reptiles] (val animal: A)






