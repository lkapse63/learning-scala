package geekforgeeks.parameterized

object ContravariantDemo {

  def main(args: Array[String]): Unit = {
      val superType = new SuperType()
      val subType = new SubType()

    val displayType =new DisplayType()
    displayType.display(superType)
    displayType.display(subType)

  }
}

abstract class Type[-T]{
  def typeName:Unit
}

class SuperType extends Type[AnyVal]{
  override def typeName: Unit = println("From Super type")
}

class SubType extends Type[Int]{
  override def typeName: Unit = println("From Sub type")
}

class DisplayType {
  def display(t : Type[Int]):Unit={
    t.typeName
  }
}