package geekforgeeks.classess

object InheritanceDemo {

  def main(args: Array[String]): Unit = {
    val scify=new Scientific()
    println("inv 4 :: ",scify.inv(4))
  }

}

class Calculator {
   val add = (x:Int, y: Int) => x + y
   val sub = (x:Int, y: Int) => x - y
   val mul = (x:Int, y: Int) => x * y
   val div = (x:Double, y: Double) => x / y
}

class Scientific extends Calculator{
  val inv =  div(1,_:Double)
  val sin = (x:Double) => Math.sin(x)
}
