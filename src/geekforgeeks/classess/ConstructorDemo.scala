package geekforgeeks.classess

class ConstructorDemo(id:Int, name:String, addr:Address) {

   def this(){
     this(0,"",null)
   }

  def this(id:Int, name:String) = this(id,name,null)

  override def toString: String =""+id+" "+name+" "+ addr

}

class Address(pin:Int){

  override def toString: String = pin.toString
}

object ConstructorDemoMain{


  def main(args: Array[String]): Unit = {
    val obj = new ConstructorDemo(1,"abc")
    val addr= new Address(440008)
    val obj2 = new ConstructorDemo(1,"abc2",addr)
    println(obj)
    println(obj2)
  }
}

