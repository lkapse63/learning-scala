package geekforgeeks.classess

object AbstractDemo {

  def main(args: Array[String]): Unit = {
     println("shift by 2 (16): ",new Scientific().sift(16))
    println("dog class :: ", new Dog("filipa").toString)
  }

  abstract class Calculator {
    val add = (x:Int, y: Int) => x + y
    val sub = (x:Int, y: Int) => x - y
    val mul = (x:Int, y: Int) => x * y
    val div = (x:Double, y: Double) => x / y

    def sift(num : Int):Int
  }

  class Scientific extends Calculator{
    val inv =  div(1,_:Double)
    val sin = (x:Double) => Math.sin(x)

    override def sift(num: Int): Int = num >> 2
  }

  abstract class Animal(breed : String)

  class Dog(name: String) extends Animal(name){
    override def toString: String = "Dog{name:"+name+"}"
  }

}
