package geekforgeeks.classess

class GenericMap[A,B] {

  private class MapNode[A,B](key:A,value:B){
    var nextNode : MapNode[A,B] = _
    override def toString: String = key.toString+" : "+value.toString
    def getKey = key
    def getValue = value

  }

  private var node:MapNode[A,B] = _

  def put(key:A,value:B): Unit = {
    var n = new MapNode[A,B](key,value)
    //Check for already exist
    var tmpNode : MapNode[A,B]=getNode(key)
     if(tmpNode != null ){
       tmpNode = n
       println("Node found ",tmpNode)
     }else{
       n.nextNode=node
       node=n
     }
  }

  private def printEntry(node: MapNode[A,B]): Unit ={
    if(node != null){
      println(node)
      printEntry(node.nextNode)
    }
  }


  def printAll(): Unit ={
    printEntry(node)
  }
  var tmp : B= _
  def get(key:A):B={
    import scala.util.control.Breaks._
    var travelNode=node
    breakable(
       while (true) {
         if(travelNode ==null )
           break()
         if (key.equals(travelNode.getKey)) {
           tmp= travelNode.getValue
           break()
         }
         travelNode = travelNode.nextNode
       }
    )
    tmp
  }



  private def getNode(key:A):MapNode[A,B]={
    import scala.util.control.Breaks._
     var travelNode:MapNode[A,B]=node
    breakable(
      while (true) {
        if(travelNode ==null )
          break()
        if (key.equals(travelNode.getKey)) {
          break()
        }
        travelNode = travelNode.nextNode
      }
    )
     travelNode
  }

}

object mainAppMap{
  def main(args: Array[String]): Unit = {
     val genericMap= new GenericMap[Int, String]()
      genericMap.put(1,"abc")
      genericMap.put(2,"xyz")
      genericMap.put(1,"pqr")
      genericMap.printAll()
//    println(genericMap.getNode(2).equals(()))
  }
}
