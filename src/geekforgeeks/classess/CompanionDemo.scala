package geekforgeeks.classess

object CompanionDemo {

  def main(args: Array[String]): Unit = {
       Shape("Tringle").paint()
       Shape().paint()
  }

  class Shape(name:String){
//    var name:String=""
    val paint = () => println(name+" painted red")
  }

  object Shape{
    def apply(name:String): Shape = {
      val shape=new Shape (name)
//      shape.name = name
      shape
    }

//    def apply(): Shape = new Shape ()
    def apply(): Shape = new Shape ("" )
  }




}
