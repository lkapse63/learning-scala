package geekforgeeks.classess

trait Game[A] {

  def start[A] :Unit

  def play[A](min : A ) : A
}

class Cricket extends Game[Int]{
  override def start[Int]: Unit = println("criket starting...")

  override def play[Int](min:Int): Int = {
     println("playing cricket..",min.getClass)
     val playtime=min.asInstanceOf[Int]
     val addTime=playtime + ""+20
     addTime.asInstanceOf[Int]
  }
}

class LinkedList[A]{
    private var head: Node[A] = _

    def addNode(node : A) {
      val n =new Node(node)
      n.next=head
      head=n
    }

    private def printNode(node : Node[A]): Unit ={
      if(node!=null){
        println(node)
        printNode(node.next)
      }
    }

   def printAll(): Unit ={
     printNode(head)
   }

   class Node[A](elem : A){
     var next: Node[A] = _
     override def toString: String = elem.toString
   }

}

object mainApp{
  var tmp : Int = _
  def main(args: Array[String]): Unit = {
     val cricket =new Cricket();
//    println(cricket.play(10))

    val linkedInt= new LinkedList[Int]()
    linkedInt.addNode(1)
    linkedInt.addNode(2)
    linkedInt.printAll()

    val linkedString= new LinkedList[String]()
    linkedString.addNode("abc")
    linkedString.addNode("xyz")
    linkedString.printAll()

    println("empty variable tmp :: ",tmp)

  }
}