package geekforgeeks.classess

object InnerClassDemo {

  def main(args: Array[String]): Unit = {
    val inner= new InnerClassDemo.Inner()
    val power=inner.power(2,3)
    println("Power ",power)
  }

  def factorial(num : Int): Int={
      if(num ==0 ) 1 else num * factorial(num-1)
  }

  class Inner {
      def power(base : Int, exp : Int): Int ={
          var tmp=1;
          for(i <- 1 to exp) tmp = tmp * base
          tmp
      }
  }

}
