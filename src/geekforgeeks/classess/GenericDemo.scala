package geekforgeeks.classess

object GenericDemo {
  def main(args: Array[String]): Unit = {
    val casio =new Casio()
    println(casio.add(10,20))
    val primo =new Primo()
    println(primo.mul(10,20))
  }
}

abstract class Calculator2[A]{
  def add(num1 : A ,num2 : A) : A
  def sub(num1 : A ,num2 : A) : A
  def mul(num1 : A ,num2 : A) : A
}

class  Casio extends Calculator2[Int]{
  override def add(num1: Int, num2: Int): Int = num1 + num2
  override def sub(num1: Int, num2: Int): Int = num1 - num2
  override def mul(num1: Int, num2: Int): Int = num1 * num2
}

class Primo extends Calculator2[Double]{
  override def add(num1: Double, num2: Double): Double = num1 + num2
  override def sub(num1: Double, num2: Double): Double = num1 - num2
  override def mul(num1: Double, num2: Double): Double = num1 * num2
}

abstract  class Mario[A,B]{
  def add(num1 : A ,num2 : B) : B
}

class Luigee extends Mario[Int,Double]{
  override def add(num1: Int, num2: Double): Double = num1 + num2
}

abstract class Odessy[A]{
  def walk(steps : A) : A {}
}