package geekforgeeks.classess

object RecursionDemo {
  def main(args: Array[String]): Unit = {
    val arr = Array[Int] (1,2,3,4,5,6,7,8,9)
    val str = Array[String] ("abc","xyz","pqr","jkl","mno")
    val itr =new ArrayIterator[Int]()
    itr.printArr(arr)

    val itr2 =new ArrayIterator[String]()
    itr2.printArr(str)
  }
}

class  ArrayIterator[A]{

  def printArr(arr: Array[A], index: Int = 0): Unit = {
    if (arr.length <= index) {
      println()
    }
    else {
      println(arr(index));
      printArr(arr, index + 1)
    }
  }


}
