package exceptions

import scala.util.control.Exception.Finally

object ExceptionDemo {
  
  def main(args:Array[String]){
    
    var a=10
    var b=0
    try{
      a/b
//      throw new ArithmeticException("Divide by zero")
    }catch{
      case e: ArithmeticException => println("ArithmeticException: "+e.getMessage)
      case ex: Exception => println("Exception: "+ex)
      
    }finally{
      println("Finally block")
    }
    
    
    println("Rest code")
    try{
    println(sum(1,2,3))
    }
    catch{
      case e : CustomeException => println(e.getMessage())
    }
  }
  
  @throws(classOf[NumberFormatException])
  @throws(classOf[CustomeException])
  def sum(array : Int*) : Int ={
    var s=0;
    for(a <- array)
      s+=a
    s
//    throw new NumberFormatException()
    
    throw new CustomeException("... mera");
  }
}