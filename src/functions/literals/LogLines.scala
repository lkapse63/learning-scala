package functions.literals

object LogLines {

  def getLines() = {
   scala.io.Source
     .fromFile( new java.io.File("./resources/sample.log"))
     .getLines()
  }
}
