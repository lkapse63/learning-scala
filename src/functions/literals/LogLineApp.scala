package functions.literals

object LogLineApp {

  def main(args: Array[String]): Unit = {
   val logProcess = LogProcess()
    println("get finished time :: "+logProcess.getFinishedTime())
    println("get errors \n"+logProcess.getError())
    println("most occurred line  group by time")
    logProcess.numberOccurance()
      .foreach(println _)  // partially applied function

  }

}
