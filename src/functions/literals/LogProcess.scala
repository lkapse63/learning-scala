package functions.literals

class LogProcess {

  private val substringLogLine = (line: String) => line.substring(0, line.lastIndexOf("]") + 1)
  private val isFinished = (line: String) => line.contains("finished")
  private val isError = (line: String) => line.toLowerCase.contains("error")
  private val splitLine = (line: String) => {
    val split = line.split("]")
    (split(0) + "]", split(1))
  }
  private val logCounter = (logTime: String, groupedLine: Seq[(String, String)]) => (logTime, groupedLine.size)

  def getFinishedTime() = {
    LogLines.getLines()
      .filter(isFinished)
      .map(substringLogLine)
      .mkString
  }

  def getError(): String = {
    LogLines.getLines()
      .filter(isError)
      .mkString("\n")
  }

  def timeAndLineMap() = {
    LogLines.getLines()
      .map(splitLine)
  }

  def group() = {
    timeAndLineMap()
      .toSeq
      .groupBy(tup => tup._1)
  }

  def numberOccurance(): Seq[(String, Int)] = {
    group()
      .map { case (l1, l2) => logCounter(l1, l2) }
      .toSeq
      .sortBy(-_._2) // sort the seq in descending order
  }

}

object LogProcess {
  def apply(): LogProcess = new LogProcess()
}
