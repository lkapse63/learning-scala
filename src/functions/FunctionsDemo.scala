package functions

object FunctionsDemo {
  
  //Scala function syntax
/**  def functionName(functionParameter: parameterType) : returnType ={
      // function body
  }*/
  
  
  def function1( parm1 : Int) : Boolean ={
     true;
  }
  
  //void function
  def voidFun(){
    println("Void simple function");
  }
  
  //return value
  def rtnFun()={
    var a=10 
    a
  }
  
  //param functions
  def paramFun(a: Int, b: Int) : Int={
    val sum=a+b
    sum
  }
  
  
  //Recursive function
  
  def recursiveFun(a : Int, b : Int) : Int ={
    
    if(b==0) //Terminate condiation
      0
    else
     a+ recursiveFun(a,b-1)
  }
  
  //Default value functions
  def defaultFun(a : Int =10 ,b : Int =20)={
    val sum= a+b;
    sum
  }



  def multiArguments(x:String*): Unit ={
    x.foreach(println)
  }

  def main(args : Array[String]){
    println(function1(10));
    voidFun();
    println(rtnFun());
    println(paramFun(10,20))
    println("Recursive function demo "+recursiveFun(2,3))
    println("Defult value function demo "+defaultFun())
    
    // Scala Function Named Parameter Example
    println("function name param :: "+paramFun(a=10,b=20))
    println("function name param :: "+paramFun(b=10,a=20))

    multiArguments("col1","col2","col3")
    val list = "col1" :: "col2" :: "col3" :: Nil
    multiArguments(list: _*)
  }
}