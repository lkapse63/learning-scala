package functions

object PartialFunctions {

  def main(args: Array[String]): Unit = {
    val list: List[Double] = List(4, 16, 25, -9)
    val sublist: List[Int] = List(4, 16, 25, -9)

    list.map(x => x+10)
    def sub = (x : Int) => x - 10
    def div = (x : Double) => x / 10

    println("cus map:: "+cusmap(sub,sublist))
    println("cus map:: "+cusmap(div,list))


  }

  def partialSqrt : PartialFunction[Double,Double] ={
    case d: Double if d > 0 => Math.sqrt(d)
  }


  /**
    * Creating custome map function
    */
  def cusmap[A](f: (A) => A, list :List[A]):List[A]={
      for{
        x <- list
      }yield f(x)
  }

}
