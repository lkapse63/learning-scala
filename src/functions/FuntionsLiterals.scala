package functions

object FuntionsLiterals {

  def main(args: Array[String]): Unit = {

    /**
      * Calling function
      */
    val fun_1=fun1("hello")
    fun_1("lucky")

    fun2("hello")("lucky 2")

//    val f3=fun3("hello","")
//    println(f3)

    fun4("hello")("lucky 4")



  }

  /**
    * Function returning function
    * @param prefix
    * @return
    */

  def fun1(prefix:String) ={
    (tail : String) => println(prefix + "  "+ tail)
  }

  /**
    * Method 2
    */
   val fun2 = (prefix:String) => {
     (tail : String) => println(prefix + "  "+ tail)
   }

  /**
    * method 3
    */
//  val fun3:(String)=>String = (prefix : String) => (tail : String) => prefix + "  "+ tail


  /**
    * method 4
    */
  val fun4 = (prefix : String) => (tail : String) => println(prefix + "  "+ tail)

}
