package functions

object HighOrderFun {

  def main(args: Array[String]) {
    /**
     * Scala Example: Passing a Function as Parameter in a Function
     */
    println("sqr o/p " + sqrCaller(10, sqr))

    /**
     * Function compostions
     */
    val result = sqr(add2(10.2).toInt)
    println("fun compostion :: " + result)

    //Ananymous functions
    anonymoyusFun()

    //function curring
    funCurringDemo()
    
    // Variable lenght demo
    variableLen(1,2,3,4,5,6,7,8,9,10);

    //
    showSumX()
    showAreaOp()
  }

  def sqr(a: Int): Int = {
    a * a
  }

  def sqrCaller(b: Int, fun: Int => Int): Int = {
    fun(b)
  }

  //Function Compositions

  def add2(a: Double): Double = {
    a + 2
  }

  /**
   * Scala Anonymous (lambda) Function
   */

  def anonymoyusFun() {
    var fun1 = (a: Int, b: Int) => a + b

    println("fun1 :" + fun1(10, 20))

    var fun2 = (_: Int) + (_: Int)

    println("fun2 wild cards :" + fun2(10, 20))
  }

  /**
   * Scala Function Currying
   *
   * In other words it is a technique of transforming a function that takes multiple arguments
   * into a function that takes a single argument.
   */

  def funCurringDemo() {
    val fun = addCurring(10)_
    val fun2 = fun(10)
    println("fun2 :" + fun2)
  }

  def addCurring(a: Int)(b: Int) = {
    a + b
  }
  
  /**
   * Scala Function with Variable Length Parameters
   */
  
  def variableLen(params : Int*) : Unit ={
    var sum : Int=0
    for( elem <- params)
      sum += elem
      
      println("Variable len o/P "+sum)
  }

  /**
   * Higher order function
   * will accept the function as input parameter
   * will return a function form another function
   */

  def sumX(num:Double, f:(Double)=>Double) ={
     f(num)
  }
  /**
   * Calling sumX function in
   * Different way
   */
  def showSumX(): Unit ={
    sumX(10, (x:Double) => x*x + x*x)
    sumX(10, (x) => x*x + x*x)
    sumX(10, x => x*x + x*x)
    val f = (x:Double) => x * x
    sumX(10, f)
  }

  /**
   * Return from high order function
   */
  def areaOp(constant : Double ) ={
    (radius : Double) => radius * radius * constant
  }
  def areaOp2(constant : Double ) ={
    val f =(radius : Double) => radius * radius * constant
     f
  }
  def areaOp3(constant : Double ) ={
    def f(radius : Double) = radius * radius * constant
    f _
  }

  def showAreaOp()={
    val carea = areaOp(3.14 )
    println("Area of circle 2.4 :: ",carea(2.4))

    println("Area of circle 2.4 :: ",areaOp3(3.14)(2.4))

  }

  def printArr(arr: Array[Int], index: Int = 0): Unit = {
    if (arr.length <= index) {
      println()
    }
    else {
      println(arr(index));
      printArr(arr, index + 1)
    }
  }
}