package functions

class HighOrderExist[A] {

  def found: Boolean = true

  def exist(lst: List[A], p: A => Boolean, index: Int = 0): Boolean = {
    if (index >= lst.size) false
    else{
      if(p(lst(index))){
          found
      }else{
        exist(lst, p, index + 1)
      }
    }
  }
}

  object HighOrderExist {
    def apply[A](): HighOrderExist[A] = new HighOrderExist[A]()

    def main(args: Array[String]): Unit = {
      val lst = List(1, 2, 3, 4, -2, 6, 7, 8, 9, 2)
      val highOrderExist = HighOrderExist[Int]()
      println(highOrderExist.exist(lst, _ == -1))
      println("size of lst :: " + lst.size)

      val stringLst = List("abc","pqr","lmn","xyz")
      val highOrderExistString = HighOrderExist[String]

      println("string exist (lmn)::"+ highOrderExistString.exist(stringLst, _.equals("lmn")))
      println("string greater than length 3 ::"+ highOrderExistString.exist(stringLst, _.length > 3))
    }
  }


