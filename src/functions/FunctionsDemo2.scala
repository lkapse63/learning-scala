package functions


object FunctionsDemo2 {


  def main(args: Array[String]): Unit = {

    val v = 2
    val v2 = 2;
    //    val ans= caller(v,v2, div)

    //    println("ans out:: "+ans)
    val l = List(1, 2, 3, 4, 5, 6, 7, 8, 9)

    //    myItr(l)
    //    println("facto:: " + sum2(10,20,30))

    val d = l.map(x => x + 10)
      .filter(_ > 14)
      .reduce(_ + _)
      .*(2)
      ./(2)

    val inv = div(1, _: Double)
    def mycube:(Double) => Double =  (x:Double)=>{x*x*x}:Double
    val mycube2= (x:Double) => x*x*x
    val f=(n:Int) => n + 20
    println("fun call : "+f(2))
//   def funcall(f:(Int) )= f(10):Int
    val cube=mycube2(_:Double)
    print(cube)
  }


  /**
    * Caller functions
    *
    * @param v
    * @param f
    * @return
    */
  def caller(v: Int, v2: Int, fun: (Int, Int) => Int) = fun(v, v2)

  /**
    * Power fucntions
    *
    * @param v
    * @return
    */
  def mul(v: Int, v2: Int) = v * v2


  /**
    * Adding
    *
    * @param v
    * @param v2
    * @return
    */
  def add(v: Int, v2: Int) = v + v2

  /**
    * Subtracting
    *
    * @param v
    * @param v2
    * @return
    */
  def sub(v: Int, v2: Int) = v - v2

  /**
    * Division
    *
    * @param v
    * @param v2
    * @return
    */
  def div(v: Double, v2: Double) = v / v2

  def myItr(l: List[Int]) = l.foreach((v) => println(v * v))

  (v: Int) => println(v * v)

  def facto(n: Int): Int = {
    if (n == 1) 1 else n * facto(n - 1)
  }

  def sqr = (n:Int) => (n * 2):Int



  /**
    * Anonymous Functions
    */

  val sum1 = (a: Int, b: Int) => a + b

  val sum2 = (_: Int) + (_: Int) + (_: Int)

  val sum3 = sum4((a: Int, b: Int) => a + b)

  val sum5 = sum4(_ + _)

  def sum4(fun: (Int, Int) => Int): Int = {
    fun(10, 5)
  }





}
