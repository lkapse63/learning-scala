package loops
import scala.util.control.Breaks._
object LoopsDemo {

  def main(args: Array[String]) {
    var a = 1;

    //while loop
    while (a < 20) {
      println(a)
      a = a + 2;
    }

    //do while
    a = 1
    do {
      println("do while:: " + a)
      a = a + 2;
    } while (a < 20)

    //For loop
    /*for( i <- range){
        // statements to be executed
      }  */

    for (i <- 1 until 10) {
      println("printing until for loop " + i)
    }

    for (i <- 1 to 10) {
      println("printing to for loop " + i)
    }
    
    for (i <- 1 to 10 if i%2==0) {
      println("printing mod if for loop " + i)
    }
    
    for (i <- 2 to 10 by 2) {
      println("printing by for loop " + i)
    }
    
//    Scala for-loop Example by using yield keyword
      var result = for( a <- 1 to 10) yield a 
      breakable{
        for(i<-result){  
            println(i)  
            break
        }  
      }
        
      
//      printing collections
      
      val list= List(1,2,3,4,5,6,7,8,9)
      
      for(i <- list)
       println("print list: "+i)
       
       list.foreach(println)
       
       breakable {
        list.foreach{
        break
        println
        }
      }
       
      
      list.foreach((element:Int)=>print("anything:: "+element+" "))  
  }
}