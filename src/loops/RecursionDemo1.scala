package loops

import scala.util.control.Breaks.break

object RecursionDemo1 {

  val list : List[Int] = (1 to 100 ).toList

  def findMid1(list : List[Int]) : Int = {
    var item = -1
    for(l <- 0 to list.length / 2 -1){
        item = list(l)
      }
    item
  }

  def findMid2(list : List[Int]) : Int = {
    if(list.length <= 1) list(0) else list(list.length/2 - 1)
  }

  def findMid3(list : List[Int]) : Int = {
    var num = -1
    var index = 0
    while ((list.length / 2) > index){
      num = list(index)
      index += 1
    }
    num
  }
  def findMid4(list : List[Int]) : Int = {
    if(list.length <= 1) list(0)
    else{
      val sublist = for(i <- 1 to list.length / 2) yield i
      /**
       * Reverse list
       */
      val revers = for(j <- sublist.length  to 1 by -1 ) yield list(j)

      revers(0)
    }
  }

  /**
   * Scala way with recursion
   */
  def findMid5(list : List[Int], index: Int = 0) : Int = {
    if(list.length <=1 || index >= list.length / 2 -1) list(index) else findMid5(list, index+1)
  }

  def makeRowSeq(row : Int, upto : Int =10) = for(col <- 1 to upto) yield {
    val prod = (row * col).toString
    val padding =  " " * (4 - prod.length)
    padding + prod
  }

  def makeRow(row : Int, upto : Int = 10) : String = makeRowSeq(row,upto).mkString

  def multiTbl(upto : Int=10) = {
    val tbl=for(i <- 1 to 10) yield makeRow(i,upto)
    tbl.mkString("\n")
  }

  /**
   * Closure
   */
  def takeFromList(f: Int => Boolean): List[Int] = list.takeWhile(f)

  def personHolder(p: Person): Double => Double = {
    p match {
      case p1 if p1.age > 60 => (x: Double) =>   0.9
      case p1 if p1.age < 60 && p1.age > 50 => (x: Double) => p.age * x * 0.3
      case p1 if p1.age < 50 && p1.age > 40 => (x: Double) => p.age * x * 0.2
      case _ => (x: Double) => p.age * x * 0.1
    }
  }

  /**
   * Find sum of list
   * Using Recursion
   */
  def sum(list:List[Int]) : Int = list match {
    case Nil => 0
    case x :: xs => x +  sum(xs)
  }
  /*
  scala> sum(List[Int](1,2,3,4,5,6,7,8,9))
  val res0: Int = 45

   */

  case class Person(name:String,age:Int)

  /**
   *
   * @param args
   */
  def main(args: Array[String]): Unit = {
    println("findMid1 :: "+ findMid1(list))
    println("findMid2 :: "+ findMid2(list))
    println("findMid3 :: "+ findMid3(list))
    println("findMid4 :: "+ findMid4(list))
    println("findMid5 :: "+ findMid5(list))

    println(multiTbl(10))

    println("closure "+takeFromList(_ < 25))

    val p1 = Person("lucky",65)
    val p2 = Person("lucky",55)
    val p3 = Person("lucky",45)
    val p4 = Person("lucky",25)

    val p1fun=personHolder(p1)
    val p2fun=personHolder(p2)
    val p3fun=personHolder(p3)
    val p4fun=personHolder(p4)
    println("p1 bonus "+p1fun(100))
    println("p2 bonus "+p2fun(100))
    println("p3 bonus "+p3fun(100))
    println("p4 bonus "+p4fun(100))
  }

}
