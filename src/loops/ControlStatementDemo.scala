package loops

import java.io.File

object ControlStatementDemo {

  def main(args: Array[String]): Unit = {
    println("gcd 66 , 42 :: "+gcdLoop(66,42))
    println("gcd 66 , 42 :: "+gcd(66,42))

    //read files name in direcotry
    val fileNames = (new java.io.File(".")).listFiles()

    for(i <- 0 until fileNames.length )
      println(fileNames(i))

    val getFiles = (file: File) => file.listFiles()

    for(file <- fileNames;
        if file.isDirectory;
        if(file.getName.equalsIgnoreCase("src"));
        name <- getFiles(file).filter(f => f.getName.endsWith(".scala"))
        )
      println(scala.io.Source.fromFile(name).getLines().mkString("\n"))

   val srcCode= for{
      file <- fileNames
      if file.isDirectory
      if file.getName.equalsIgnoreCase("src")
      srcFile <- getFiles(file).filter(f => f.getName.endsWith(".scala"))
      src = scala.io.Source.fromFile(srcFile).getLines().mkString("\n")
    } yield src

    println(srcCode.mkString("\n"))


  }



  def gcdLoop(x:Long, y:Long):Long={
    var a =x
    var b = y
    while(a!=0){
      val tmp=a
      a= b % a
      b=tmp
    }
    b
  }

  def gcd(x:Long,y:Long) : Long = if(y==0) x else gcd(y, x % y)



}
