package abstracts

abstract class Employee {
  
  def basic() : Int
  
  def log() : Unit =println("Employee abstract class")
}