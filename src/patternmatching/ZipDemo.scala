package patternmatching

object ZipDemo {


  def main(args: Array[String]): Unit = {
    val list = (1 to 10).toList.map(x => (math.random * x * 100).toInt)
    println(s"list : $list")

    list.zip(Stream from 1)
      .map {
        case (i, j) if j > 5 => (i, j)
        case _ => ()
      }
      .filter {
        case (i, j) => true
        case _ => false
      }
      .map {
        case tuple@(a: Int, b: Int) => tuple
      }
      .map {
        t => (t._1.asInstanceOf[Int] , fun(t._2.asInstanceOf[Int]))
      }
      .map( tf => tf._2(tf._1))
      .filter( x => filtercond(fun(x)) > 1000.0)
      .foreach {
        x => println(x)

      }
  }

  def fun(x:Int) = (y:Int) => x * y
  def filtercond(f : (Int)=> Int) = f(1) * 0.5
}
