package patternmatching
import Math.{E,PI}
object Demo1 {

  def main(args: Array[String]): Unit = {
    val num = Number(10.1)
    println(num)
    val unOp = UnOp("-", num)
    println(UnOp("-", unOp))
    println("return function\n",
      simplyFyingTop(UnOp("-", UnOp("-", UnOp("*", BinOp("*", unOp, unOp))))).asInstanceOf[Expr],
      "\n",
      simplyFyingTop(Variable(Variable(Variable("y").name).name)),
      "\n describe \n",
      describe(Demo1)
    )

    println("Pi matcher :: " + piMatcher(E))
    println("instance pattern 10.1 " + instancePattern(10.1).getClass)
    seqPattern(List(1, 2, 3, 4))
    tupuleDemo((10, 20))

    println("Variable binding")
    println(simplyFyingTop(
      (unOp, UnOp("+", BinOp("*", unOp, unOp)))
    ))

    println("Simplying all")
    val expr = simplifyAll(UnOp("-", UnOp("-", UnOp("*", BinOp("*", unOp, unOp)))))
    println(expr)

    val capitals = Map("france" -> "paris", "india" -> "delhi")
    val capital = getOptionValue(capitals get "india")
    println("capital of india : " + capital)


    println(s"Second function ${second(List(1, 2, 3, 4))}")
    println(s"Second function ${second(List())}")
    println(s"Second function ${second(Nil)}")
    println(s"Second function ${second(List(1))}")

    val results = List(Some("apple"), None,
      Some("orange"))

    // filter while iterating over a loop
    for(Some(res) <- results)
      println(res)

    // filter while iterating over a loop
    val capList = List(Capitals("delhi","india"),None)
    for(Capitals(city,country) <- capList)
      println(city,country)
  }

  def simplyFyingTop(expr:Any)  = expr match {
    case BinOp(_, _, _) => expr              // wild card matching
    case UnOp("-", UnOp("-", exprs)) => exprs
    case Variable(e) => e
      // Variable binding demo
    case (UnOp(_,_),UnOp("+", e @ BinOp("*", _,_))) => e
    case _ =>
  }

  /**
   * constant pattern match
   */

  def describe(x:Any) :String = x match {
    case 5 => "Five"
    case true => "Boolean true"
    case Nil => "Empty list"
    case Demo1 => "Demo1 object"
    case _ =>"Nothing"
  }

 val pi=PI

  def piMatcher(x:Any) : String = x match {
    case pi => "Strange math..? "+pi
    case PI => "Strange PI..? "+PI
    case _ => "Ok"
  }

  /**
   * Instance pattern
   */
  def instancePattern(x:Any): Any = x match {
    case x: Int => x.asInstanceOf[Int]
    case x : Double => x.asInstanceOf[Double]
    case _ =>
  }

  /**
   * Sequence pattern
   */
  def seqPattern(x :Any)  = x match {
    case List(1,_,_) => println("found it")
    case List(1,_*) => println("found it 2")
    case _ =>
  }

  /**
   * Tuple demo
   */
  def tupuleDemo(x:Any) = x match {
    case (a,b) => println("two tuple")
    case (a,b,c) => println("three tuple")
    case _ =>
  }

  /**
   * Pattern guard demo
   */

  def patternGaurdeDemo(x:Any) = x match {
    //case BinOp("+", x ,x ) => BinOp("*",x, Number(2)) // wrong can not resolved x
    case BinOp("+", x ,y ) if x==y => BinOp("*",x, Number(2)) // using pattern gaurd
    case _ =>
  }

  def simplifyAll(expr: Expr): Expr = expr match {
    case UnOp("-", UnOp("-",e)) =>
      simplifyAll(e) // is its own inverse
    case BinOp("+", e, Number(0)) =>
      simplifyAll(e) // ‘0’ is a neutral element for ‘+’
    case BinOp("*", e, Number(1)) =>
      simplifyAll(e) // ‘1’ is a neutral element for ‘*’
    case UnOp(op, e) =>
      UnOp(op, simplifyAll(e))
    case BinOp(op, l, r) =>
      BinOp(op, simplifyAll(l), simplifyAll(r))
    case _ => expr
  }

  def describe(e: Expr): String = (e: @unchecked)  match {
    case Number(_) => "a number"
    case Variable(_) => "a variable"
  }

  /**
   * Get option value
   */
  def getOptionValue(option: Option[String]) = option match {
    case Some(x) => x
    case None => "None"
  }


  val second : List[Int] => Int = {
    case x :: y :: _ => y
    case List() => 0
    case _ => -1
  }

  /**
   * Partial function
   */
  val partialSecond : PartialFunction[List[Int],Int] ={
    case x :: y :: _ => y
  }

  //partial function illustration
  new PartialFunction[List[Int],Int] {
    override def isDefinedAt(x: List[Int]): Boolean = x match {
      case x :: y :: _ => true
      case _ => false
    }

    override def apply(v1: List[Int]): Int = v1 match {
      case x :: y :: _ => y
    }
  }

}




sealed abstract class Expr

case class Variable(name: String) extends Expr
case class Number(num: Double) extends Expr
case class UnOp(operator:String , args : Expr) extends Expr
case class BinOp(operator:String , left : Expr, right: Expr) extends Expr


case class Capitals(city:String, country:String)