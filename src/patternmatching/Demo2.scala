package patternmatching

object Demo2 {

  val opGroups = Array(
     Set("|", "||"),
      Set("&", "&&"),
      Set ("ˆ"),
      Set("==", "!="),
      Set("<", "<=", ">", ">="),
      Set("+", ""),
      Set("*", "%")
  )

  val precedance = {
    val acoss = for {
      i <- 0 until opGroups.length
      op <- opGroups(i)
    } yield op -> i
    Map() ++ acoss
  }

  def main(args: Array[String]): Unit = {
    println(precedance)

    val m = for {
      i <- 0 until 5
    } yield  randomIntRange(-10,10)

    println("map "+(List() ++ m))
  }

  def randomIntRange(min: Int , max :Int) : Int = {
    (Math.random() * (max - min) + min).asInstanceOf[Int]
  }

  val randomRange = (min:Double, max:Double) => Math.random() * ( max -min) + min
}
