package patternmatching

import scala.io.Source

object ReadFileDemo {

  def main(args: Array[String]): Unit = {

    val widthOfLength = (s:String) => s.length.toString.length
    val lines=Source.fromFile("src/patternmatching/Demo2.scala").getLines().toList;

    val longestLine = lines.reduceLeft( (a,b) => if (a.length > b.length) a else b )

    val maxWidth = widthOfLength(longestLine)

    for(line <- lines){
      val numSpec = maxWidth - widthOfLength(line)
      val padding = " " * numSpec
      println(padding + line.length +" | "+ line)
    }
    println("maxWidth "+maxWidth)
    println("longestLine "+longestLine)
  }

}
