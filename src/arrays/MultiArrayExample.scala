package arrays

class MultiArrayExample {
  /**
   * Multidimensional Array Syntax
   * var arrayName = Array.ofDim[ArrayType](NoOfRows,NoOfColumns) or  
		 var arrayName = Array(Array(element?), Array(element?), ?)  
   */
   var arr = Array(Array(1,2,3,4,5), Array(6,7,8,9,10),
             Array(16,17,18,19,20))   // Creating multidimensional array  
    def show(){  
        for(i<- 0 to arr.length-1){               // Traversing elements using loop  
           for(j<- 0 to 4){  
                print(" "+arr(i)(j))  
            }  
            println()  
        }      
    }  
}  
  
object MainMultiArrayExample{  
    def main(args:Array[String]){  
        var a = new MultiArrayExample()  
        a.show()                       
    }  
}