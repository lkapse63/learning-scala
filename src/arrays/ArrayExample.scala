package arrays

class ArrayExample {
  
  /**
   * Array Creation syntax
   * 
   * var arrayName : Array[arrayType] = new Array[arrayType](arraySize);   or  
     var arrayName = new Array[arrayType](arraySize)  or  
		 var arrayName : Array[arrayType] = new Array(arraySize);   or  
		 var arrayName = Array(element1, element2 ? elementN)  
		 
   */
  
  
  var arr = new Array[Int](5)         // Creating single dimensional array  
    def show(){  
        for(a<-arr){                      // Traversing array elements  
            println(a)  
        }  
        println("Third Element before assignment = "+ arr(2))        // Accessing elements by using index  
        arr(2) = 10                                                          // Assigning new element at 2 index  
        println("Third Element after assignment = "+ arr(2))  
    }  
}  
  
object MainObject{  
    def main(args:Array[String]){  
        var a = new ArrayExample()  
        a.show()  
    }  
}