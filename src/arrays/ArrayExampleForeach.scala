package arrays

class ArrayExampleForeach {
  def show(arr:Array[Int]){  
          arr.foreach(println)
    }  
}  
  
object MainArrayExampleForeach{  
    def main(args:Array[String]){  
        var arr = Array(1,2,3,4,5,6)    // creating single dimensional array  
        var a = new ArrayExampleForeach()  
        a.show(arr)                     // passing array as an argument in the function  
    }  
}