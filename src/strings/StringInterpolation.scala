package strings

object StringInterpolation {
  
  def main(args : Array[String]){
    
    var pi=3.14
    
    println(s"value of pi = $pi")
    
    var s="This is sparta "   
    var f=2.0
    println(f"$s%s $f%2.1f")
    
    var s3 =raw"this \t is \n sparta"
    println(s3)
  }
}