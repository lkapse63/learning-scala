package strings

object StringsDemo {
  
  def main(args : Array[String]){
    stringFun()
  }
  
  def stringFun() :Unit={
    val s1:String="This is saprta....."
    var s2:String="This is saprta.....";
    println(s1==s2)
    println("equals : "+s1.equals(s2))
    
//    /Rest are java string methods

    val num : Int = 10

    val oct = 0x1d
    val nov = 0x1ff
    val dec = 0xd1
    val a = 'A'
    val c = '\u0042'
    println(c)

    println(
      """|Welcome to Ultamix 3000.
         |Type "HELP" for help.""".stripMargin)


    def allIndex(str: String, c : Char, index: Int): Int ={
      if(index == -1) index else {
        println(str.indexOf(c, index))
        allIndex(str, c, str.indexOf(c, index+1))
      }
    }
    println(allIndex("this is a string",'s',0))

    def unary_!(num:Int) = ~ num

    println(unary_!(2))
  }


}