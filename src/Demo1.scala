

object Demo1 {

  def main(args: Array[String]) {
    lazy val a = 10;
    println(a)
    var b: Int = a + 10;
    println(b)

    val data: String = "hi hello"
    println("String data: " + data);
    val c: Char = 'L';
    println("Char :" + c);
    val e: Char = '\u0102'
    println("Char :" + e);

    if (a > 21) {
      println("if executed")
    } else {
      if (a > 20) {
        println(" else if executed")
      }
      else{
        println(" else else executed")
      }
    }
    
    
    //MEthod calling
    val result = checkIt(-10)  
      println (result)  

  }
  
// scala way
//  def checkIt (a:Int)  =  if (a >= 0) 1 else -1    // Passing a if expression value to function
  
  //old way
  
  def  checkIt (a:Int): Int={
    if (a >= 0) 
       1 
    else 
        -1 
  }
  
  def matching(s: Any)= s match {

    case 1       => println("One")
    case "hello" => println("Hello")
    case 'c'     => println("c char")
    case _       => println("None")
  }

}


 
