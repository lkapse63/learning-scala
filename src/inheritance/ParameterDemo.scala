package inheritance

object ParameterDemo {

  def main(args : Array[String]): Unit ={
    println("Starting....")

    val arr = Array.fill(10)(round(2)(util.Random.nextDouble()*100))
    bubbleSort(arr)(_>_)
  }

  def bubbleSort[A]( arr : Array[A])(op: (A,A)=>Boolean) : Unit ={

    println("element of an array => "+ arr.mkString(","))

     for( i <- 0 until(arr.length-1)) {
       for(j <- 0 until(arr.length-i-1)){
          if(op(arr(j),arr(j+1))){
            val tmp = arr(j)
            arr(j) = arr(j+1)
            arr(j+1) =tmp
          }
       }
     }
    println("sorted element of an array => "+ arr.mkString(","))
  }



  val round  = (p:Int) => (n:Double) => {
    val s = math.pow(10, p)
    math.round(n * s) / s
  }


}
