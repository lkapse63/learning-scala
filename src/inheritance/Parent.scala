package inheritance

class Parent {
   val basic: Double=20000.6
   
   final def log() = println("parent class")
}