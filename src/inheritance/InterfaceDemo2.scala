package inheritance

import inheritance.InterfaceDemo2.Mammal

object InterfaceDemo2 {

  trait Animal

  trait Herbivorus extends Animal
  trait Carnivorus extends Animal

   trait Mammal extends Animal{
     def hasGland = s"${this.getClass.getSimpleName} has glands"
  }

  class Person(name:String, age:Double) extends Mammal with Herbivorus with Carnivorus

  class Tiger(age:Double)  extends Mammal with Carnivorus

  def main(args: Array[String]): Unit = {
    val tiger = new Tiger(8)
    val person = new Person("lucky",31)
    println(tiger.hasGland)
    println(person.hasGland)
  }
}
