package inheritance.book

object App {
  def main(args: Array[String]): Unit = {
    val arrayElements:Elements = new ArrayElements(Array("elem1","elem2","elem3","elem4"))
    println("arrayElements : "+arrayElements.height)
    println("arrayElements : "+arrayElements.width)
    println("arrayElements : "+arrayElements.contents.mkString("[",",","]"))

    val lineElements: Elements = new LineElements("this is a line")
    println("lineElements : "+lineElements.height)
    println("lineElements : "+lineElements.width)
    println("lineElements : "+lineElements)

    val uniformElements : Elements = new UniformElements('c',3,9)
    println("uniformElements : "+uniformElements)

    draw(arrayElements)
    draw(lineElements)
    draw(uniformElements)

    println("beside call")
    lineElements.beside(uniformElements).draw()
  }

  def draw(e : Elements) = e.draw()

}
