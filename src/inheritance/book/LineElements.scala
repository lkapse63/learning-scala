package inheritance.book

class LineElements(s : String) extends Elements{
  override val  contents : Array[String] =  Array(s)
  override def height: Int = super.height
  override def width: Int = s.length

  override def draw(): Unit = println(s"drawing LineElements contents $this")
}
