package inheritance.book

import inheritance.book.Elements.elem

abstract class Elements {
  def contents : Array[String]
  def height : Int = contents.length
  def width : Int = if (height == 0) 0 else contents(0).length

  def draw(): Unit = println(s"drawing Elements contents $this")

  def above(that : Elements) : Elements = {
    val this1 = this.widen(that.width)
    val that1 = that.widen(this.width)
    new ArrayElements(this1.contents ++ that1.contents)
  }

  def beside(that : Elements) : Elements ={
    val this1 = this.heighten(that.height)
    val that1 = that.heighten(this.height)
    elem(
      for( (line1,line2) <- this1.contents zip that1.contents) yield line1 + line2
    )
  }

  def widen(w : Int) : Elements =
    if(w <= width) this
    else {
      val left = elem(' ', (w - width) / 2, height)
      val right = elem(' ', (w - width - left.width) , height)
      left.beside(this).beside(right)
    }

  def heighten(h : Int) : Elements =
    if(h <= height) this
    else {
      val top = elem(' ',width, (h-height)/2)
      val bottom = elem(' ', width, h - height - top.height)
      top.above(this).above(bottom)
    }

  override def toString: String = contents mkString "\n"
}

object Elements {
  def elem(contents : Array[String]) = new ArrayElements(contents)
  def elem(line : String) = new LineElements(line)
  def elem(ch : Char, width : Int,height : Int) = new UniformElements(ch, width, height)
}
