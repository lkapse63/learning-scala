package inheritance.book

class UniformElements(ch : Char, override val width : Int, override val height: Int) extends Elements {
  private val line = ch.toString * width
  override def contents: Array[String] = Array.fill(height)(line)
  override def draw(): Unit = println(s"drawing UniformElements contents $this")
}
