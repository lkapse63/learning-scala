package inheritance.book

/*case class ArrayElements(conts : Array[String]) extends Elements{
  override def contents: Array[String] = conts
}*/
case class ArrayElements(override val contents : Array[String]) extends Elements
