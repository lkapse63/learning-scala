package inheritance

import scala.collection.mutable.ArrayBuffer


object LetsTrait {

  trait Animal {
    def hasBone = "no bone"
  }

  trait  Vertebrates extends Animal {
    override def hasBone: String = "Bone present"
  }

  trait  InVertebrates extends Animal {
    override def hasBone: String = "No bones"
  }

  trait ColdBlood extends  Vertebrates {
     def booldType ="cold blood"
  }

  trait HotBlood extends  Vertebrates {
    def booldType ="Hot Blood"
  }

  trait Mammals extends HotBlood {
    def hasMilkGland = "yes"
    def canFly = "Yes"
  }

  trait Bird extends HotBlood{
    def hasMilkGland = "No"
    def canFly = "Yes"
  }
  abstract class Bird2 extends HotBlood{
    def hasMilkGland = "No"
    def canFly = "No"
  }

  class Bat extends Bird2 with  Mammals with Bird {

    override def hasMilkGland: String = super.hasMilkGland
    override def canFly: String = super.canFly
  }

  trait MathOp[T] {
    def add(x : T, y : T)  : T = throw new NotImplementedError("not supported")
    def minus(x : T, y : T) : T = throw new NotImplementedError("not supported")
    def mul(x : T, y : T) : T = throw new NotImplementedError("not supported")
    def div(x : T, y : T) : T = throw new NotImplementedError("not supported")
  }

  trait DoubleOp extends MathOp[Double] {
    override def add(x : Double, y : Double) = x + y
    override def minus(x : Double, y : Double) = x - y
    override def mul(x : Double, y : Double) = x * y
    override def div(x : Double, y : Double) = x / y
  }

  class IntOp extends DoubleOp {

  }

  class StringOp extends MathOp[String] {
    override def add(x : String, y : String) = x + y
  }

  /**
   *
   * Trait orders
   */

  trait IntQue{
    def get() : Int
    def put(x : Int)
  }

  class BasicIntQue extends IntQue{
    val arr : ArrayBuffer[Int] = new ArrayBuffer[Int]()
    override def get(): Int = arr.remove(0)
    override def put(x : Int) = {arr += x}
  }

  trait DoublingQue extends  IntQue {
    abstract override def put(x: Int): Unit = { super.put(2 * x)}
  }

  trait IncrementQueue extends IntQue{
    abstract override def put(x: Int): Unit = { super.put(1 + x)}
  }

  trait FilteringQueue extends IntQue{
    abstract override def put(x: Int): Unit = {
      if(x >= 0)
       super.put(x)}
  }

  class MyQueue extends BasicIntQue with DoublingQue

  def main(args: Array[String]): Unit = {
    val bat = new Bat();
    println(bat.booldType)
    println(bat.hasBone)
    println("canFly "+bat.canFly)
    println("hasMilkGland "+bat.hasMilkGland)

    val intOp = new IntOp()
    println("intOp add : :"+intOp.add(1,2))

    val stringOp = new StringOp()
  //  stringOp.mul("abc","xyz") //Runtime Error

   val myQueue = new MyQueue()
   myQueue.put(1);
   myQueue.put(2);
    println("myqueue get "+myQueue.get())
    println("myqueue get "+myQueue.get())

    // defing new class on the go
    val queue = (new BasicIntQue  with IncrementQueue with FilteringQueue)
    queue.put(-1)
    queue.put(0)
    queue.put(1)
    println("queue get "+queue.get)
    println("queue get "+queue.get)
//    println("queue get "+queue.get)
  }


}
