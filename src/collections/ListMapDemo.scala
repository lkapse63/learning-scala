package collections
import scala.collection.mutable._

object ListMapDemo {
  
  def main(args:Array[String]){  
        var listMap = ListMap("Rice"->"100","Wheat"->"50","Gram"->"500")    // Creating listmap with elements  
        listMap.foreach{  
            case(key,value)=>print(key+"->"+value+"\t")  
        }  
        println("\t")  
        var newListMap = listMap+("Rice"->"550")  
        newListMap.foreach {  
            case (key, value) => print (key + " -> " + value+"\t")  
        }     
    }  
  
}