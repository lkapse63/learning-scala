package collections

import scala.collection.SortedSet

object SetDemo {
  
  
  def main(args: Array[String]){
    
    var set1=Set("tushar","umesh","lucky")
    println("Set content: "+set1)
    println("Set seq: "+set1.seq)
    println("Set empty: "+set1.isEmpty)
    println("Set size: "+set1.size)
     
    set1 +="madhu"
    println("madhu added to Set: "+set1)
    
    set1 -="madhu"
    println("madhu removed from Set: "+set1)
    
    
    println("madhu contains in Set: "+set1.contains("madhu"))
    var set2: Set[String] = Set()
    println("Empty Set: "+set2)
    
    set2 +="madhu"
    set2 +="ranjan"
    println("New Set : "+set2)
    
    var set3=set1++set2
    println("sets added: "+set3)
    
    //iterating sets elementsusing for ech    
    set1.foreach((elem : String) => println(elem))
    
    //Set Oparations
    println("set intersections: "+set1.intersect(set2))
    println("set intersections by & oprator: "+(set1 & set2))
    println("set unions: "+set1.union(set2))
    
    //sorted set
    var sorted = SortedSet(set1.toSeq :_ *)
    println("sorted set "+sorted)
  }
}