package collections

object VectorDemo {
  def main(args:Array[String]){  
        var vector = Vector("Hocky","Cricket","Golf")  
        var vector2 = Vector("Swimming")  
        print("Vector Elements: ")  
        vector.foreach((element:String) => print(element+" "))  
        var newVector  = vector :+ "Racing"                             // Adding a new element into vector  
        print("\nVector Elements after adding: ")  
        newVector.foreach((element:String) => print(element+" "))  
        var mergeTwoVector = newVector ++ vector2                       // Merging two vector  
        print("\nVector Elements after merging: ")  
        mergeTwoVector.foreach((element:String) => print(element+" "))  
        var reverse = mergeTwoVector.reverse                            // Reversing vector elements  
        print("\nVector Elements after reversing: ")  
        reverse.foreach((element:String) => print(element+" "))  
        var sortedVector = mergeTwoVector.sorted                        // Sorting vector elements  
        print("\nVector Elements after sorting: ")  
        sortedVector.foreach((element:String) => print(element+" "))  
    }  
}