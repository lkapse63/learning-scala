package collections
import scala.collection.immutable.BitSet

object BitSetDemo {
  
  def main(args : Array[String]){
    
    var bitSet= BitSet(1,2,3,4,5,6,7,8,9,10)
    println("immutable : "+bitSet)
    
    bitSet-=10;
    println("immutable : "+bitSet)
    
    bitSet+=12;
    println("immutable : "+bitSet)
    
    println("immutable size : "+bitSet.size)
  }
}