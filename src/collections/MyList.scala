package collections

import scala.annotation.tailrec


object List2 {

  class List2[A](val elem: A, var nextElem: List2[A] = null) {
    def head: A = elem

    def tail: List2[A] = nextElem


    def this(elem: A) = this(elem, null)

    def forEach(f: A => Unit): Unit = {
      var that = this
      while (that.tail != null) {
        f(that.head)
        that = that.tail
      }
      f(that.head)
    }

    def add(elem: A): Unit = {
      val node = new List2[A](elem)
      var that = this
      while (that.tail != null) that = that.tail
      that.nextElem = node
    }

    def removeAt(index: Int): Unit = {
      var that = this
      (1 until (index)).foreach(i => {
        that = that.nextElem
      })
      that.nextElem = that.nextElem.nextElem
    }

    def get(index: Int): A = {
      var that = this
      for (i <- 1 until (index)) {
        if (that.tail == null)
          throw new ArrayIndexOutOfBoundsException(s"index more than list size $index")
        else that = that.tail
      }
      that.head
    }

    def contains(elem: A): Boolean = {
      var that = this
      while (that.tail != null) {
        if (that.head == elem) return true

        that = that.tail
      }
      false
    }

    def find(f: A => Boolean): Option[A] = {
      var that = this
      while (that.tail != null) {
        if (f(that.head)) return Some(that.head)
        that = that.tail
      }
      None
    }

    def filter(p: A => Boolean): List2[A] = {
      var that = this
      var list: List2[A] = null;
      while (that.tail != null) {
        if (p(that.head) && list == null) list = new List2[A](that.head)

        else if (p(that.head)) list.add(that.head)

        that = that.tail
      }
      list
    }

    def map[B](p: A => B): List2[B] = {
      var that = this
      var list: List2[B] = null;
      while (that.tail != null) {
        if (list == null) list = new List2[B](p(that.head))
        else list.add(p(that.head))
        that = that.tail
      }
      list.add(p(that.head))
      list
    }

    def foldLeft[B](z: B)(op: (A, B) => B): B = {
      var acc = z
      var that = this
      while (that.tail != null) {
        acc = op(that.head, acc)
        that = that.tail
      }
      acc = op(that.head, acc)
      acc
    }

    def append(that: List2[A]): List2[A] = {
      var merge: List2[A] = this
      while (merge.tail != null) {
        merge = merge.tail
      }
      merge.nextElem = that
      this
    }



    final def reverse(that: List2[A] = this): List2[A] = {
      if (that.tail == null) new List2[A](that.head)
      else {
        ::(reverse(that.tail),new List2[A](that.head))
      }

    }

    private def append2(first: List2[A], second: List2[A]): List2[A] = {
      var merge: List2[A] = first
      while (merge.tail != null) {
        merge = merge.tail
      }
      merge.nextElem = second
      first
    }

    def :: (first: List2[A], second: List2[A]): List2[A] = {
      var merge: List2[A] = first
      while (merge.tail != null) {
        merge = merge.tail
      }
      merge.nextElem = second
      first
    }

    def mkString(sep: String, list2: List2[A] = this): String = {
      if (list2.nextElem == null) s"${list2.elem}" else list2.head + sep + mkString(sep, list2.tail)
    }

    def mkString(sep: String, prefix: String, suffix: String, list2: List2[A]): String = {
      s"$prefix${mkString(sep, list2)}$suffix"
    }


    def size: Int = {
      var size = 0
      var that = this
      while (that.tail != null) {
        size += 1
        that = that.nextElem
      }
      size + 1
    }

    override def toString: String = mkString(",", "[", "]", this)
  }

  def copy[A](seq: Seq[A]): List2[A] = {
    val node: List2[A] = new List2[A](seq.head)
    for (elem <- seq.tail) yield node.add(elem)
    node
  }

  def main(args: Array[String]): Unit = {
    val list = new List2[Int](1)
    list.add(2)
    list.add(3)
    list.add(4)
    list.add(5)
    list.forEach(println)
    println("before remove")
    println(list)
    println("after remove at 2")
    list.removeAt(2)
    println(list)
    println("size " + list.size)

    println("list from seq")
    val list2 = copy((1 to 1000))
    println("size " + list2.size)
    println("element at 15 " + list2.get(1))
    println("contains 100 " + list2.contains(100))
    println("find > 100 " + list2.find(_ > 100))
    println("filter  " + list2.filter(_ < 10))

    list2
      .filter(_ < 10)
      .map((x: Int) => x * 1.0)
      .forEach(println)

    println("Fold left " + list2.filter(_ < 10).foldLeft[Int](0)(_ + _))

    println("append " + list.append(new List2[Int](8)))

    println("Reverse : "+list.reverse())

  }


}
