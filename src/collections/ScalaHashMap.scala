package collections
import scala.collection.mutable._

object ScalaHashMap {
  
   def main(args:Array[String]){  
        var hashMap = HashMap("A"->"Apple","B"->"Ball","C"->"Cat")  
        hashMap.foreach {  
            case (key, value) => println (key + " -> " + value)       // Iterating elements  
        }  
        println(hashMap("B"))               // Accessing value by using key  
        
        var newHashMap = hashMap+("D"->"Doll")  
        
        
        newHashMap.foreach {  
            case (key, value) => println (key + " -> " + value)  
        }  
          
    }  
}