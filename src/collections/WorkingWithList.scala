package collections

import collections.WorkingWithList.wrapWithHash

import scala.annotation.tailrec

object WorkingWithList {

  /**
   * Construct a list
   */
  val numList : List[Int] = List(1,2,3,4,5,6,7,8,9)
  val stringList : List[String] = "abc" :: "xyz" :: "pqr" :: Nil
  case class Person(name:String, age : Int)
  val arrayList : List[Person] = Array(Person("lucky",29), Person("Prajakta",29)).toList
  val empty = List()

  // Basic op on list
  val numHead : Int = numList.head
  val stringTail : List[String] = stringList.tail
  val isEmpty : Boolean = empty.isEmpty

  // Sorting algorithm
  def isort(xs : List[Int]) : List[Int] = {
    println("isort calling " + xs.mkString(", "))
    if (xs.isEmpty) Nil
    else {
      insert(xs.head, isort(xs.tail))
    }
  }
    def insert(x : Int, xs : List[Int]): List[Int] = {
      println("data from insert "+x +" list tail "+xs.mkString(", "))
      if(xs.isEmpty || x <= xs.head){
        println(s"insert from if xs.isEmpty  ${xs.isEmpty}  x <= xs.head  ${ if (!xs.isEmpty) x <= xs.head}")
        val retList = x :: xs
        println("if return list "+retList.mkString(", "))
        retList
      }
      else {
        println("return from insert else "+xs.head)
        xs.head :: insert(x,xs.tail)
      }
    }

  /**
   * Pattren matching with list
   */
  val listnum = List(1,2,3,4)
  /*val List(a,b,c,d) = listnum
   a =1
   b =2
   c =3
   d =4
   */

  val a::b::c::d::e:: rst = List(1,2,3,4,5,6,7)
  /*
    val a: Int = 1
    val b: Int = 2
    val c: Int = 3
    val d: Int = 4
    val e: Int = 5
    val rst: List[Int] = List(6,7)
   */

  /**
   * Insertion sort with pattern matching
   */
  def isortPm(list : List[Int]) : List[Int] = list match {
    case List() => List()
    case head :: tail => insertPm(head, tail)
  }

  def insertPm(elem: Int, list: List[Int]) : List[Int] = list match {
    case List() => List(elem)
    case head :: tail => if( elem <= head) elem :: list
                         else  head :: insertPm(elem, tail)
  }
 /*
      isortPm(List(5,4,3,2,1))
     o/p =>  List(4, 3, 2, 1, 5)
o  */

  /**
   * List concatination ::: / ++
   */
  val mergeList =List(1,2,3,4,5) ::: List(6,7,8,9)
//  val mergeList: List[Int] = List(1, 2, 3, 4, 5, 6, 7, 8, 9)
  /*
  scala> val mergeList=List(1,2,3,4,5) ++ List("abc","xyz")
  val mergeList: List[Any] = List(1, 2, 3, 4, 5, abc, xyz)
   */

  def append[T](xs : List[T], ys : List[T]) : List[T] = xs match {
    case List() => ys
    case head :: tail => head :: append(tail,ys)
  }
  /*
  scala> append(List(1,2,3),List(4,5,6))
  val res21: List[Int] = List(1, 2, 3, 4, 5, 6)
   */

  /*
  last element and expect last
    scala> list.last
    val res23: Int = 6

    scala> list.init
    val res24: List[Int] = List(1, 2, 3, 4, 5)

   */
  /**
   * Reverse a list
   */
  numList.reverse
 // val res25: List[Int] = List(6, 5, 4, 3, 2, 1)

  def myReverse[T](list : List[T])  = for(i <- list.length until  0 by -1) yield list(i)

  /**
   * Map function for list
   */

  def map[A,B](f : A => B, seq: Seq[A]) : Seq[B] = {
    for {
      x <- seq
    } yield f(x)
  }
  //val list = List(1,2,3,4,5,6,7,8,9)
  //map((x:Int) => x * 2, list)
  //Seq[Int] = List(2, 4, 6, 8, 10, 12, 14, 16, 18)

  /**
   * Filter function
   */
  def filter[A](seq: Seq[A], f : A => Boolean) : Seq[A] = {
    for {
      x <- seq
      if f(x)
    } yield x
  }
  //val list = List(1,2,3,4,5,6,7,8,9)
//  filter(list, (x:Int) => x * 2 > 8 )
//  val res6: Seq[Int] = List(5, 6, 7, 8, 9)

  /**
   * Custom while loop
   */

  def whilst(testCondition: => Boolean)(blockOfCode: => Unit) : Unit ={
     while(testCondition){
       blockOfCode
     }
  }

  //Use of above code
  var i=0
  whilst( i < 5){
    print(s"$i \t")
    i +=1
  }
  //0       1       2       3       4

  def ifBothTrue(test1: => Boolean)(test2: => Boolean)(blcCode: => Unit): Unit ={
    if(test1 && test2)
      blcCode
  }

  val age=21
  val accident=0
  ifBothTrue( age > 18)(accident ==0){
    println("discount given")
  }

  /**
   * Function return something
   */
  def validUser(testLogin : => Boolean)(blcCode: => String): String = {
    if(testLogin)
      blcCode
    else "Not valid user"
  }

  validUser( true) {
    "User is valid"
  }

  /**
   * Implicit demo
   */
  def printIfIntTrue(a : Int)(implicit b : => Boolean) = {
    if(b) println(a)
  }

  /**
   * Function that return another function
   */
  @tailrec
  def fun(a:Int,b:Int):  (Int,Double) => Double = {
        if(b <= 1) {
          println("executing if ",a,b)
          (x:Int,y:Double) => x * addFun(a,b) * y
        }
        else {
          println("executing else ",a,b-1)
          fun(addFun(a,b), b - 1)
        }
  }

  def addFun(a:Int,b:Int) = a*b

  /**
   * Partially applied function
   */
  def wrap(str1:String)(str2:String)(str3:String) = str1+str2+str3

  def wrapWithHash = wrap("#")(_:String)("#")
  def startWith$ = wrap("$")(_:String)("")
  def separateWithFileSeparator = wrap(_:String)("/")(_:String)

  def wrapHasAndStart$(str:String) = wrap("$")(wrapWithHash(str))("")
  def endWithAmp = wrap("")(_:String)("&")
  def appendWithAmp(str:String) = endWithAmp(str)
}
