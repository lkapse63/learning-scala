package collections

object ListHigOrderFunction {

  def main(args: Array[String]): Unit = {

    val personList = List(
       Person("lucky","pune"),
       Person("prajakta","pune"),
       Person("praful","nagpur"),
       Person("umesh","mumbai")
    )

    System.out.println("Person list")
    personList.foreach(println)

    System.out.println("\nPune person list")
    for(p <- personList if p.addr.contains("pune"))
      System.out.println(p)
  }

  case class Person(name:String, addr:String)


}
