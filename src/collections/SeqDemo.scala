package collections

object SeqDemo {
   def main(args:Array[String]){  
        var seq:Seq[Int] = Seq(52,85,1,8,3,2,7,8)  
//        seq.foreach((element:Int) => print(element+" "))  
        println("\nis Empty: "+seq.isEmpty)  
        println("Ends with (2,7): "+ seq.endsWith(Seq(2,7)))  
        println("contains 8: "+ seq.contains(8))  
        println("first index of 8 : "+seq.indexOf(8))  
        println("last index of 8 : "+seq.lastIndexOf(8))  
        println("Reverse order of sequence: "+seq.reverse)  
    }  
}