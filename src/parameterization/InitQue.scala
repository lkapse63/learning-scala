package parameterization

class InitQue[T](private val elem : List[T]) {

  def head: T = elem.head
  def tail : InitQue[T] = new InitQue(elem.tail)
  def append(x : T) = new InitQue[T]( x ::  elem)

  override def toString: String = elem.toString()

  override def hashCode(): Int = elem.hashCode()

}
