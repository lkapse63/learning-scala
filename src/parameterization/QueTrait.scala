package parameterization

trait QueTrait[T] {

  def head : T
  def tail : QueTrait[T]
  def append(x : T) : QueTrait[T]

}

object QueTrait {

  def apply[T](x : T*): QueTrait[T] = new QueTraitImpl[T](x.toList)

  class QueTraitImpl[T](elem: List[T]) extends QueTrait[T] {
    override def head: T = elem.head
    override def tail: QueTrait[T] = new QueTraitImpl(elem.tail)
    override def append(x: T): QueTrait[T] = new QueTraitImpl(x :: elem)
  }

}


