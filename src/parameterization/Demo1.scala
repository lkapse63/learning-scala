package parameterization

object Demo1 {

  def main(args: Array[String]): Unit = {

    val que=new InitQue[Int]( List(1,2,3,4,5,6))
    println(que.head)
    println(que.tail.toString)
    println(que.append(7))

    val que2 = new Que2[Int]( List(1,2,3,4,5), List())

    println(que2.head)
    println(que2.tail)
    println(que2.append(6))

     PrivateQue[Int](1,2,3,4,5,6,7);
     PrivateQue[Int](List(1,2,3,4,5,6,7));

    /**
     * Companion object
     */
    new PrivateQue[Int](Set(1,2,3,4,5,6,7));

    val qt =QueTrait[Int](1,2,3,4,5,6,7);
    println(qt.head)



  }

}
