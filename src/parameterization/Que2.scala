package parameterization

class Que2[T] (
              private val leading : List[T],
              private val trailing : List[T]
              ) {

    private def mirror = if(leading.isEmpty) new Que2[T]( trailing.reverse,Nil) else this

  def head = mirror.leading.head
  def tail = new Que2[T]( mirror.leading.tail ,mirror.trailing)
  def append (x :T) = new Que2[T](leading, x :: trailing)

  override def toString: String = leading.toString() + "\t" + trailing.toString()
}
