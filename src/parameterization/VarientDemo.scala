package parameterization

object VarientDemo {

  def main(args: Array[String]): Unit = {
    val c1 = new Cell[String]("v1")
    val c2 : Cell[AnyRef] = c1
//    c2.set(new InitQue[Int](List(1,2)))
    c1.get
  }

  class  Cell[+T] (init : T){

    private [this] var current = init

    def get = current
//    def set (x:T ) = {current=x }

  }
}
