package parameterization

class PrivateQue[T] private (private val elem : List[T]) {

  def head = elem.head
  def tail = new PrivateQue[T]( elem.tail )
  def append(x:T) = new PrivateQue[T](elem ::: List(x))

  def this(x : Set[T]) = this(x.toList)

}

object PrivateQue {
  def apply[T](elem: T*): PrivateQue[T] = new PrivateQue(elem.toList)
  def apply[T](elem: List[T]): PrivateQue[T] = new PrivateQue(elem)
}
