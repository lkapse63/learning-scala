package oop

object ClassesApp {

  def apply(name: String, age: Int, address: Address): ClassesApp = new ClassesApp(name, age, address)

  def apply(name: String): ClassesApp = new ClassesApp(name)

  def apply(name: String, age: Int): ClassesApp = new ClassesApp(name, age)

}

case class Address(street: String, city: String, pin: Int)

class ClassesApp(private val name: String, private val age: Int, private val address: Address) extends Ordered[ClassesApp] {

  //Auxiliary constructor 1
  def this(name: String, age: Int) = this(name, age, null)

  //Auxiliary constructor 2
  def this(name: String) = this(name, 0, null)

  def Name: String = name

  def Age: Int = age

  def Address: Address = address

  def incrementAge: ClassesApp = ClassesApp.apply(Name, Age + 1, Address)

  def updateAddress(address: Address): ClassesApp = ClassesApp.apply(Name, Age, address)

  def updateName(name: String): ClassesApp = ClassesApp.apply(name, Age, Address)

  def updateAge(age: Int): ClassesApp = ClassesApp.apply(Name, age, Address)

  override def toString: String = s"ClassesApp=(name=$Name, Age=$Age, Address=$Address)"

  override def compare(that: ClassesApp): Int = this.hashCode().compareTo(that.hashCode())

  override def hashCode(): Int = 31 * 7 + (Age + (if (Name != null) Name.hashCode else 0) + (if (Address != null) Address.hashCode() else 0))

  override def equals(obj: Any): Boolean = {
    if (obj == null) return false
    if (getClass != obj.getClass) return false
    val other: ClassesApp = obj.asInstanceOf[ClassesApp]
    other.hashCode() == this.hashCode()
  }


}

object App {
  def main(args: Array[String]): Unit = {
    val classAppLucky = ClassesApp("lucky", 31)
    println(classAppLucky)
    val luckyAgeIncrement = classAppLucky.incrementAge
    println(luckyAgeIncrement)
    val luckyAddressUpdate = classAppLucky.updateAddress(Address("Maan road", "Pune", 411057))
    println(luckyAddressUpdate)

    val pList = List(classAppLucky, luckyAgeIncrement, luckyAddressUpdate)
    println("Sorted working...!")
    pList.sorted.foreach(println)

    //    println(classAppLucky.hashCode())
    //    println(luckyAddressUpdate.hashCode())
    //    println(luckyAgeIncrement.hashCode())

  }
}


