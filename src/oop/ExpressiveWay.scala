package oop

object ExpressiveWay extends App{

  trait Action {
    def  init() = println("init done")
    def apply(x : Int) : Int
  }

  val myAction : Action = (x : Int) => x + 1

  println(myAction(5))

  new Thread(() => println("I am running easy")).start()
}
